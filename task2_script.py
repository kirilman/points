import trimesh
import matplotlib.pyplot as plt

import numpy as np

import os
import torch
from model import PointNetCls
from torch.utils.data import DataLoader

import argparse 

class points_dataloader():
    def __init__(self, dataset_dir):
        files = os.listdir(dataset_dir)
        self.files = []
        self.labels = []
        for f in files:
            self.files += [os.path.join(dataset_dir,f)]
                
    def __getitem__(self, idx):

        mesh = trimesh.load_mesh(self.files[idx])
        #нормализация в начало координат
        points = np.array(mesh.vertices)
        mean = points.mean(axis=0)
        points = points - np.expand_dims(mean,0)
        scale  = (points**2).sum(axis=1).max()
        points = points/scale
        n_pnts = len(points)
        inpt = torch.tensor(points, dtype = torch.float).T
        return inpt, self.files[idx]
        
    def __len__(self):
        return len(self.files)

if __name__ == "__main__":

    parser = argparse.ArgumentParser('Classification')
    parser.add_argument('--dataset_dir', type=str, default='', help='batch size in training')
    args = parser.parse_args()
    args.dataset_dir
    
    dataset = points_dataloader(args.dataset_dir) 
    loader = DataLoader(dataset,16)
    
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = PointNetCls(6)
    d = torch.load('model_851.pt', map_location=torch.device(device))
    model.load_state_dict(d)

    arr_pred = []
    file_names = []
    for el in loader:
        inpt = el[0]
        print(inpt.mean())
        files = el[1]
        pred = model(inpt)[0]
        argmax = pred.argmax(axis=1).cpu().numpy().tolist()
        print('pred',argmax)
        arr_pred = arr_pred + argmax
        file_names = file_names + list(files)
    
    
    out_file = open('result.csv','w')

    for l, f in zip(arr_pred, file_names):
        out_file.write("{},{}\n".format(f.split('/')[-1],l))

    out_file.close()


