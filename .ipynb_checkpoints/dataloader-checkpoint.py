import torch
import os
import trimesh
from torch.utils.data import DataLoader
import numpy as np

class points_dataloader():
    def __init__(self, dataset_dir, is_train):
        dirs = os.listdir(dataset_dir)
        self.files = []
        self.labels = []
        self.class_ids = {'cone':0,'cube':1, 'cylinder':2, 'plane':3, 'torus':4, 'uv_sphere':5}
        for d in dirs:
            if is_train:
                sub_dir = os.path.join(dataset_dir, d,'train')
                sub_files = os.listdir(sub_dir)
            else:
                sub_dir = os.path.join(dataset_dir, d, 'test')
                sub_files = os.listdir(sub_dir)
            for f in sub_files:
                self.files += [os.path.join(sub_dir,f)]

            self.labels += [self.class_ids[d] for i in range(len(sub_files))]
        print('Количество примеров: ', len(self.files),' ', len(self.labels))
                
    def __getitem__(self, idx):
        self.files[idx]
        mesh = trimesh.load_mesh(self.files[idx])
        #нормализация в начало координат
        points = np.array(mesh.vertices)
        mean = points.mean(axis=0)
        points = points - np.expand_dims(mean,0)
        scale  = (points**2).sum(axis=1).max()
        points = points/scale
        n_pnts = len(points)
        inpt = torch.tensor(points, dtype = torch.float).T
        ans = {'inpt':inpt, 'label': torch.tensor(np.repeat(self.labels[idx], n_pnts), dtype = torch.long)}
        return ans
        
    def __len__(self):
        return len(self.files)

loader = points_dataloader('dataset', True)
ans = loader.__getitem__(12)

d = DataLoader(loader, batch_size=10)

s = iter(d)
i = next(s)

print(i['label'].shape)


