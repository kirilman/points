import torch
import os
import trimesh
from torch.utils.data import DataLoader
import numpy as np
"""
    Загрузчик данных
"""
class points_dataloader():
    def __init__(self, dataset_dir, is_train, for_cls):
        dirs = os.listdir(dataset_dir)
        self.files = []
        self.labels = []
        self.class_ids = {'cone':0,'cube':1, 'cylinder':2, 'plane':3, 'torus':4, 'uv_sphere':5}
        self.for_cls = for_cls
        self.is_train = is_train
        for d in dirs:
            if is_train:
                sub_dir = os.path.join(dataset_dir, d,'train')
                sub_files = os.listdir(sub_dir)
            else:
                sub_dir = os.path.join(dataset_dir, d, 'test')
                sub_files = os.listdir(sub_dir)
            for f in sub_files:
                self.files += [os.path.join(sub_dir,f)]

            self.labels += [self.class_ids[d] for i in range(len(sub_files))]
        print('Количество примеров: ', len(self.files),' ', len(self.labels))
                
    def __getitem__(self, idx):
        self.files[idx]
        mesh = trimesh.load_mesh(self.files[idx])
        #нормализация в начало координат
        points = np.array(mesh.vertices)
        mean = points.mean(axis=0)
        points = points - np.expand_dims(mean,0)
        scale  = (points**2).sum(axis=1).max()
        points = points/scale
        #Шум
        if self.is_train:
            points +=np.random.normal(0, 0.02, size=points.shape) 
        n_pnts = len(points)
        inpt = torch.tensor(points, dtype = torch.float).T
        
        if self.for_cls:
            ans = {'inpt':inpt, 'label': torch.tensor(self.labels[idx], dtype = torch.long)}
        else:
            ans = {'inpt':inpt, 'label': torch.tensor(np.repeat(self.labels[idx], n_pnts), dtype = torch.long)}
        return ans
        
    def __len__(self):
        return len(self.files)


